@echo off
setlocal enabledelayedexpansion
set path="C:\Program Files\7-Zip";%path% 		REM Include 7z.

echo [101;93mWARNING^^![0m[93m This is a batch script, and will process MANY files once it's started. Please MAKE SURE you've read the README.txt and understand what this will do. If you are unsure, please close this program IMMEDIATELY^^![0m
echo [93m- Milton Plotkin[0m
echo.
REM pause
echo.

echo [101;93mSECONDARY WARNING^^![0m[93m This batch script will ONLY work for patched SCORMs created by StorylinePatch.bat.[0m
echo [93mThis batch script will create folders in the OUTPUT directory. Ensure that the OUTPUT directory is empty before you run it.[0m
echo.
pause
echo.

set _DEBUGMODE=0

REM MAIN ZIP PATCH
REM ------------------------------------------

REM For each .zip in the SCORM folder.
for /f "delims=" %%a in ('dir "SCORM\*.zip" /b /a-d') do (
	
	REM Skip .zips which don't contain "_PATCH".
	echo %%a|findstr /i /l "_PATCH">nul 2>nul
	if errorlevel 1 (
		echo [91mInvalid .zip file provided - %%a[0m
		echo [91mPlease ensure all .zips in the SCORM folder have been patched using the StorylinePatch.bat script[0m
		echo.
		pause
	) else (
		echo ZIP found:          [95m%%a[0m
		echo --------------------------------------------------------------------------------

		REM Extract the zip
		REM ------------------------------------------

		set tempName=%%~na

		REM Extract the .zip to a temporarily named folder.
		echo [93mExtracting zip: [96m%%a[0m
		if !_DEBUGMODE!==0 (
			REM Remove the temp directory if it exists first.
			if exist "OUTPUT/!tempName!" rmdir /s /q "OUTPUT/!tempName!" >nul
			REM Extract zip contents to it.
			7z x "SCORM/%%a" "-oOUTPUT/!tempName!" "*/*" -r -aoa >nul
		)

		REM Rename the unit directory.
		REM ------------------------------------------

		REM Read XML data: Read values from the imsmanifest.xml to get the unitname.
		for /f "tokens=* delims=" %%# in ('lib\xpath\xpath.bat "OUTPUT/!tempName!/Section1/meta.xml" "//project/@title"') do set "unitname=%%#"
		for /f "tokens=* delims=" %%# in ('lib\xpath\xpath.bat "OUTPUT/!tempName!/Section1/meta.xml" "//project/@courseid"') do set "courseid=%%#"
		REM Get only the course ID.
		for /f "delims=_" %%i in ("!courseid!") do set "courseid=%%i"
		
		REM Store the final folder name.
		set folderName=!courseid! - !unitname!

		echo [93mUnit folder name:[0m !folderName!
		echo.

		REM Nest the contents of the folder inside of a new "content" directory.
		REM ------------------------------------------

		REM Rename to "content"
		cd /d "OUTPUT"
		if exist "content" rmdir /s /q "content" >nul
		rename "!tempName!" "content"

		REM Make a directory with folderName.
		if exist "!folderName!" rmdir /s /q "!folderName!" >nul
		mkdir "!folderName!"

		REM Move "content" into !folderName!
		move "content" "!folderName!"
		
		REM Remove unused files
		REM ------------------------------------------
		cd /d "!folderName!"

		del /S *.xsd 2> nul
		del /S imsmanifest.xml 2> nul
		del /S index_lms.html 2> nul
		del /S index_lms_html5.html 2> nul
		del /S ioslaunch.html 2> nul
		del /S launch.html 2> nul
		del /S launcher.html 2> nul
		del /S meta.xml 2> nul
		del /S story.html 2> nul
		del /S story.swf 2> nul
		del /S story_unsupported.html 2> nul

		REM Create shortcuts for the content
		REM ------------------------------------------
		set /a sectionNum=1
		for /d %%D in (content/Section*) do (
			echo [93mShortcut .bat copied for %%D[0m
			xcopy /y "%~dp0shortcuts\Section!sectionNum!.bat" "%~dp0OUTPUT\!folderName!" > nul
			set /a sectionNum+=1
		)

		REM VERY IMPORTANT to include this (loops breaks if otherwise).
		cd /d %~dp0
	)

	echo.
)

echo [93mDone. Check the OUTPUT folder for all the processed SCORM files.[0m
echo.

pause
EXIT /B 0