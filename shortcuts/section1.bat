@echo off
setlocal enableextensions enabledelayedexpansion

set target=%~dp0content\Section1\story_html5.html

ECHO Opening learning content with the most compatible web browser available on this device...
echo.

REM Check if the file exists.
if not exist "%target%" (
    echo [91mCould not find Didasko content to load.[0m
    echo [91mPlease ensure this .exe file is located in the same folder that the "content" folder is in before you run it.[0m
    echo.
    pause
    EXIT /B 0
)

REM Check if Chrome is installed.
%SystemRoot%\System32\reg.exe query "HKLM\Software\Microsoft\Windows\CurrentVersion\App Paths\chrome.exe" >nul 2>&1
if not errorlevel 1 (
    start "" /max chrome.exe "%target%"
    EXIT /B 0
) else (
    ECHO chrome.exe not found.
)

REM Chrome not installed. Check if Firefox is installed.
%SystemRoot%\System32\reg.exe query "HKLM\Software\Microsoft\Windows\CurrentVersion\App Paths\firefox.exe" >nul 2>&1
if not errorlevel 1 (
    start "" /max firefox "%target%"
    EXIT /B 0
) else (
    ECHO firefox.exe not found.
)

REM If neither installed, open in default browser.
ECHO Opening content using the default browser.
start "" /max "%target%"

pause

EXIT /B 0
EndLocal
ECHO ON